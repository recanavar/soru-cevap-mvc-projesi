﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SoruCevap.Models
{
    public class Sorular
    {
        [Key]
        public int SorularID { get; set; }
        public int SoruSahibiID { get; set; }
        public string Soru { get; set; }
        public DateTime SoruTarihi { get; set; }
        public string Tip { get; set; }
        public virtual ICollection<Cevaplar> Cevaplar { get; set; }
        [ForeignKey(name: "SoruSahibiID")]
        public virtual KullaniciBilgileri KullaniciBilgileri { get; set; }
        //public virtual SoruTipi SoruTipi { get; set; }
        //public int SoruTipiID { get; set; }
    }
    public class SoruView
    {
        [Required]
        public string Soru { get; set; }
        public string Tip { get; set; }

    }
}