﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SoruCevap.Models
{
    public class KullaniciBilgileri
    {
        [Key]
        public int KullaniciID { get; set; }
        [Required]
        public string AdSoyad { get; set; }
        [Required]
        public string KullaniciAdi { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public byte KullaniciFoto { get; set; }
    }

}