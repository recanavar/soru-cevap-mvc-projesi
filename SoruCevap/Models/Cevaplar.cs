﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SoruCevap.Models
{
    public class Cevaplar
    {   
        [Key]
        public int CevapID { get; set; }
        public string Cevap { get; set; }
        public int CevapOySayisi { get; set; }
        public int SoruID { get; set; }

        [ForeignKey(name: "SoruID")]
        public virtual Sorular Sorular { get; set; }
    }
} 