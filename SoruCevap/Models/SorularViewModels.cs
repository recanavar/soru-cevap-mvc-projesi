﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoruCevap.Models
{
    public class SoruViewModel
    {
        public string Soru { get; set; }
        public string KullaniciAdi { get; set; }
        public DateTime SoruTarihi { get; set; }
    }

    public class AnaSayfaViewModel
    {
        public List<string> EnSonSoru { get; set; }
        public List<string> EnPopulerSoru { get; set; }
    }

}

//public class TeknolojiViewModel
//{
//    public string Soru { get; set; }
//    public string KullaniciAdi { get; set; }
//}
//public class SporViewModel
//{
//    public string Soru { get; set; }
//    public string KullaniciAdi { get; set; }
//}
//public class SaglikViewModel
//{
//    public string Soru { get; set; }
//    public string KullaniciAdi { get; set; }
//}
//public class GenelViewModel
//{
//}