namespace SoruCevap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mg2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.KullaniciBilgileri", "Password");
            DropColumn("dbo.KullaniciBilgileri", "ConfirmPassword");
        }
        
        public override void Down()
        {
            AddColumn("dbo.KullaniciBilgileri", "ConfirmPassword", c => c.String());
            AddColumn("dbo.KullaniciBilgileri", "Password", c => c.String());
        }
    }
}
