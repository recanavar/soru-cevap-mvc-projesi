namespace SoruCevap.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mgration2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.RegisterViewModel", newName: "KullaniciBilgileri");
            DropForeignKey("dbo.Sorular", "SoruTipiID", "dbo.SoruTipi");
            DropForeignKey("dbo.Cevaplar", "Sorular_SorularID", "dbo.Sorular");
            DropForeignKey("dbo.Sorular", "KullaniciBilgileri_KullaniciID", "dbo.RegisterViewModel");
            DropIndex("dbo.Cevaplar", new[] { "Sorular_SorularID" });
            DropIndex("dbo.Sorular", new[] { "SoruTipiID" });
            DropIndex("dbo.Sorular", new[] { "KullaniciBilgileri_KullaniciID" });
            DropColumn("dbo.Cevaplar", "SoruID");
            DropColumn("dbo.Sorular", "SoruSahibiID");
            RenameColumn(table: "dbo.Cevaplar", name: "Sorular_SorularID", newName: "SoruID");
            RenameColumn(table: "dbo.Sorular", name: "KullaniciBilgileri_KullaniciID", newName: "SoruSahibiID");
            AlterColumn("dbo.Cevaplar", "SoruID", c => c.Int(nullable: false));
            AlterColumn("dbo.Sorular", "SoruSahibiID", c => c.Int(nullable: false));
            AlterColumn("dbo.KullaniciBilgileri", "Email", c => c.String());
            AlterColumn("dbo.KullaniciBilgileri", "Password", c => c.String());
            CreateIndex("dbo.Cevaplar", "SoruID");
            CreateIndex("dbo.Sorular", "SoruSahibiID");
            AddForeignKey("dbo.Cevaplar", "SoruID", "dbo.Sorular", "SorularID", cascadeDelete: true);
            AddForeignKey("dbo.Sorular", "SoruSahibiID", "dbo.KullaniciBilgileri", "KullaniciID", cascadeDelete: true);
            DropColumn("dbo.Sorular", "SoruTipiID");
            DropTable("dbo.SoruTipi");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SoruTipi",
                c => new
                    {
                        SoruTipiID = c.Int(nullable: false, identity: true),
                        Teknoloji = c.String(),
                        Gundem = c.String(),
                        Spor = c.String(),
                        Saglik = c.String(),
                        Genel = c.String(),
                    })
                .PrimaryKey(t => t.SoruTipiID);
            
            AddColumn("dbo.Sorular", "SoruTipiID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sorular", "SoruSahibiID", "dbo.KullaniciBilgileri");
            DropForeignKey("dbo.Cevaplar", "SoruID", "dbo.Sorular");
            DropIndex("dbo.Sorular", new[] { "SoruSahibiID" });
            DropIndex("dbo.Cevaplar", new[] { "SoruID" });
            AlterColumn("dbo.KullaniciBilgileri", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.KullaniciBilgileri", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Sorular", "SoruSahibiID", c => c.Int());
            AlterColumn("dbo.Cevaplar", "SoruID", c => c.Int());
            RenameColumn(table: "dbo.Sorular", name: "SoruSahibiID", newName: "KullaniciBilgileri_KullaniciID");
            RenameColumn(table: "dbo.Cevaplar", name: "SoruID", newName: "Sorular_SorularID");
            AddColumn("dbo.Sorular", "SoruSahibiID", c => c.Int(nullable: false));
            AddColumn("dbo.Cevaplar", "SoruID", c => c.Int(nullable: false));
            CreateIndex("dbo.Sorular", "KullaniciBilgileri_KullaniciID");
            CreateIndex("dbo.Sorular", "SoruTipiID");
            CreateIndex("dbo.Cevaplar", "Sorular_SorularID");
            AddForeignKey("dbo.Sorular", "KullaniciBilgileri_KullaniciID", "dbo.RegisterViewModel", "KullaniciID");
            AddForeignKey("dbo.Cevaplar", "Sorular_SorularID", "dbo.Sorular", "SorularID");
            AddForeignKey("dbo.Sorular", "SoruTipiID", "dbo.SoruTipi", "SoruTipiID", cascadeDelete: true);
            RenameTable(name: "dbo.KullaniciBilgileri", newName: "RegisterViewModel");
        }
    }
}
