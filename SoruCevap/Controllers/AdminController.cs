﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SoruCevap.Models;
using System.Threading;
using System.Globalization;

namespace SoruCevap.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public void SetCulture(string dil)
        {
            if (Thread.CurrentThread.CurrentCulture.Name != dil)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(dil);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(dil);
            }
        }

        // GET: Admin
        
        public ActionResult Index(string dil)
        {
            SetCulture(dil);
            return View(db.KullaniciBilgileri.ToList());
        }

        // GET: Admin/Details/5
        //[Authorize(Roles ="Admin")]
        public ActionResult Details(int? id, string dil)
        {
            SetCulture(dil);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KullaniciBilgileri kullaniciBilgileri = db.KullaniciBilgileri.Find(id);
            if (kullaniciBilgileri == null)
            {
                return HttpNotFound();
            }
            return View(kullaniciBilgileri);
        }

        // GET: Admin/Create
        //[Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "KullaniciID,AdSoyad,KullaniciAdi,Email,KullaniciFoto")] KullaniciBilgileri kullaniciBilgileri)
        {
            if (ModelState.IsValid)
            {
                db.KullaniciBilgileri.Add(kullaniciBilgileri);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kullaniciBilgileri);
        }

        // GET: Admin/Edit/5
        //[Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id, string dil)
        {
            SetCulture(dil);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KullaniciBilgileri kullaniciBilgileri = db.KullaniciBilgileri.Find(id);
            if (kullaniciBilgileri == null)
            {
                return HttpNotFound();
            }
            return View(kullaniciBilgileri);
        }

        // POST: Admin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "KullaniciID,AdSoyad,KullaniciAdi,Email,KullaniciFoto")] KullaniciBilgileri kullaniciBilgileri, string dil)
        {
            SetCulture(dil);
            if (ModelState.IsValid)
            {
                db.Entry(kullaniciBilgileri).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kullaniciBilgileri);
        }

        // GET: Admin/Delete/5
        //[Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id,string dil)
        {
            SetCulture(dil);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KullaniciBilgileri kullaniciBilgileri = db.KullaniciBilgileri.Find(id);
            if (kullaniciBilgileri == null)
            {
                return HttpNotFound();
            }
            return View(kullaniciBilgileri);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id,string dil)
        {
            SetCulture(dil);
            KullaniciBilgileri kullaniciBilgileri = db.KullaniciBilgileri.Find(id);
            db.KullaniciBilgileri.Remove(kullaniciBilgileri);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
