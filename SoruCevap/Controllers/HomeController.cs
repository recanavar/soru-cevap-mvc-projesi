﻿using SoruCevap.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace SoruCevap.Controllers
{
    public class HomeController : Controller
    {
        public void SetCulture(string dil)
        {
            if (Thread.CurrentThread.CurrentCulture.Name != dil)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(dil);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(dil);
            }
        }

        public ActionResult Index(string dil)
        {
            SetCulture(dil);
            var model = new AnaSayfaViewModel();
            model.EnSonSoru = EnSonSorulan();
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(SoruView model,string dil)
        {
            SetCulture(dil);
            if (!ModelState.IsValid)
            {
                return View();
            }
            var db = new ApplicationDbContext();
            var kullaniciID = db.KullaniciBilgileri.Single(k => k.KullaniciAdi == User.Identity.Name).KullaniciID;
            var soru = new Sorular() {
                Soru = model.Soru,
                Tip = model.Tip,
                SoruSahibiID = kullaniciID,
                SoruTarihi = DateTime.Now
            };
            db.Sorular.Add(soru);
            db.SaveChanges();
            db.Dispose();

            if (soru.Tip == "Sağlık")
            {
                return RedirectToAction("Saglik");
            }
            if (soru.Tip == "Gündem")
            {
                return RedirectToAction("Gundem");
            }
            return RedirectToAction(soru.Tip);
        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        public ActionResult Teknoloji(string dil)
        {
            SetCulture(dil);
            var db = new ApplicationDbContext();
            var model = Sorular("Teknoloji");
            return View(model);
        }
        public ActionResult Genel(string dil)
        {
            SetCulture(dil);
            var db = new ApplicationDbContext();
            var model = Sorular("Genel");
            return View(model);

        }
        public ActionResult Spor(string dil)
        {
            SetCulture(dil);
            var db = new ApplicationDbContext();
            var model = Sorular("Spor");
            return View(model);
        }
        public ActionResult Saglik(string dil)
        {
            SetCulture(dil);
            var db = new ApplicationDbContext();
            var model = Sorular("Sağlık");
            return View(model);
        }
        public ActionResult Gundem(string dil)
        {
            SetCulture(dil);
            var db = new ApplicationDbContext();
            var model = Sorular("Gündem");
            return View(model);
        }
        public ActionResult Cevap(string dil)
        {
            SetCulture(dil);
            return View();
        }
        public List<SoruViewModel> Sorular(string tip,int sayi = 10)
        {
            var db = new ApplicationDbContext();
            var model = db.Sorular.Where(soru => soru.Tip == tip)
                .OrderByDescending(soru => soru.SorularID)
                .Join(db.KullaniciBilgileri
                , soru => soru.SoruSahibiID, kullanici => kullanici.KullaniciID
                , (soru, kullanici) => new SoruViewModel() { Soru = soru.Soru,
                 KullaniciAdi = kullanici.KullaniciAdi,SoruTarihi = soru.SoruTarihi })
                .OrderByDescending(x => x.SoruTarihi)
                .Take(sayi).ToList();
            return model;
        }
        public List<string> EnSonSorulan(int sayi = 5)
        {
            var db = new ApplicationDbContext();
            var sorular = db.Sorular.OrderByDescending(x => x.SoruTarihi).Select(x=>x.Soru)
                .Take(sayi).ToList();
            return sorular;
        }
    }
}