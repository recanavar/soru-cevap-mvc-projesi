﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SoruCevap.Startup))]
namespace SoruCevap
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
